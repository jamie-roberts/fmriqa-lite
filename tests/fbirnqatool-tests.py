import sys
sys.path.insert(0, "..")
import fmriqa.fbirnqatool as fbt

SIEMENSBXH = 'testdata/siemens/bxh.xml'
PHILIPSBXH = 'testdata/philips/bxh.xml'
GEBXH      = 'testdata/ge/bxh.xml'

def test_parse_bxh_siemens():
    dims = fbt.parse_bxh(SIEMENSBXH)
    assert dims == {'nx': 64, 'ny': 64, 'nz': 27, 'nt': 512}

def test_parse_bxh_philips():
    dims = fbt.parse_bxh(PHILIPSBXH)
    assert dims == {'nx': 64, 'ny': 64, 'nz': 27, 'nt': 600}

def test_parse_bxh_ge():
    dims = fbt.parse_bxh(GEBXH)
    assert dims == {'nx': 64, 'ny': 64, 'nz': 19, 'nt': 512}

    
