#!/usr/bin/env python

from setuptools import setup, find_packages

from distutils.cmd import Command
from glob import glob
import os
from os.path import split, join, basename, dirname, isfile
import sys
import re


# single version
with open(join(dirname(__file__), 'fmriqa', 'version.py')) as f:
    exec(f.read())


def readme(fname):
    with open(join(dirname(__file__), fname)) as f:
        text = f.read()
    return text


# nb seems to run with unchanged cwd and these paths are relative
package_name = 'fmriqa_lite'
resource_dir = 'resources'
dependencies = [
    'numpy>=1.8.2',
    'scipy>=0.13.3',
    'scikit-image>=0.10.1',
    'pydicom>=0.9.9',
    'dcmfetch>=0.1.19'
]
if sys.version_info < (2, 7):
    dependencies.append('ordereddict')

dependency_links = [
    'git+https://bitbucket.org/rtrhd/dcmfetch.git'
]


setup(
    name=package_name,
    version=__version__,
    description='Tools for fMRI QA at CRIC based on FBIRN',
    long_description=readme('README.md'),
    author='Ronald Hartley-Davies',
    author_email='R.Hartley-Davies@bristol.ac.uk',
    license='MIT',
    url='https://bitbucket.org/jamie-roberts/fmriqa-lite',
    download_url='https://bitbucket.org/jamie-roberts/fmriqa-lite/downloads/',
    packages=find_packages(),
    install_requires=dependencies,
    dependency_links=dependency_links,
    tests_require=['nose'],
    keywords="fmri qa fbirn",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 3",
        "Topic :: Scientific/Engineering :: Medical Science Apps.",
    ]
)
