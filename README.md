# fmriqa #
A python package for wrapping the BXH/Excede fMRI stability QA tools. It provides a Qt based GUI tool for fetching QA series from a [DICOM](http://medical.nema.org/) server, running the fBIRN QA analysis scripts `fmriqa_phantomqa` and saving the results to a postgres database for historical tracking of the results. DICOM server access is provided externally by the [dcmfetch](https://bitbucket.com/rtrhd/dcmfetch) package.

## Installation ##
### Prerequisites ###

 - The [dcmfetch](https://bitbucket.com/rtrhd/dcmfetch) dialog package
 - The [BXH/Excede](https://www.nitrc.org/projects/bxh_xcede_tools/) tools. 
 - A [Postgres](https://www.postgresql.org/) database server on the deployment computer (currently tested on v9)
 - The [googlevis](http://code.google.com/p/google-visualization-python/) python module and an [apache](https://httpd.apache.org/) or other httpd for the google charts viewer

### Install ###
The main python package may be installed by running

```
python setup.py install
```
or with `pip`. The package includes python versions of the main analysis procedures in the fBIRN software.
The `setup.py` also installs an executable script `fbirnqa` in the default `bin` directory.

Alternatively, a standalone version of the script may be installed to `/usr/local/bin` without the python package by running

```
make install-standalone
```
In addition, there is a google charts `cgi-bin` program can be installed to a web server executable script directory.
```
make install-web
```
will install to `/var/www/cgi-bin/`.

There is a collection of Jupyter (IPython) notebooks in the `ipynb` directory that exercises the python versions of the analysis.

### Database ###
The results are stored in a database on the system postgres server. The database should be created
by hand and r/w access granted to all users who will perform the FMRIQA analysis.
Select only permission should be granted to other users. For the web based reports this will need to include the web server user eg `apache`.
The set up is as follows using the command `psql` run as the postgres superuser (`postgres`):

- create the database `fMRIQAResults`
- create the table `qaresults`
- add user roles with write permission for the users who will use the qa system eg
  - `add user rtrhd;`
  - `grant insert,update,select on qaresults to rtrhd;`
- add roles and grant read access for the webserver user:
  - `add user www-data;`
  - `grant select on qaresults to www-data;`
- enable access from other machines by adding a line in  `pg_hba.conf` eg:
  - `host fMRIQAResults all 137.222.202.0/24        ident`

- open any firewall on the database system to allow access to port 5432 from at least the local subnet.
- change the listen address in `postgresql.conf` from `localhost` to `*` so as to listen on external interfaces.
- ensure `identd` is setup and running on all client machines
- ensure all the client firewalls are open for port 113 (`identd`) inbound 

### Reports ###
Included in the `ipynb` directory are Jupyter notebooks for generating reports:

- Single analysis report - which runs all of the local analysis using the tools in the python package (based on Glover/fBIRN).
- Historical analysis - plots of historical trends in the data saved using the graphical tool based on access to the database.

There is also a live google charts view that is served by apache. No special configuration should be
needed for this - the script `gchart_fmriqa_report.py` iinstalled in `/var/www/cgi-bin/` should be executable by apache.
Note that it depends on an external internet connection for access to the google charts api, the flash plugin installed in the client browser and an installation of the google visualization python module on the web seerver machine.

RHD 13/10/2016
