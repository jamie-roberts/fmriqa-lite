#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Application to support fMRIQA using the fBIRN protocols and analysis tools.
Depends on:
    fBIRN analysis programs (bhx subir)
    dcmfetch: Dicom query/fetch dialog to access dicom store
    qtpy: (PyQT5/PyQt4/PySide)
    psycopg2: database connectivity (postgres)
R. Hartley-Davies October 2016
'''

# TODO: comments, docs and help
# TODO: the sliders don't seem to be greyed out when the panel is inactive
# TODO: sort out installation and database location
# TODO: which parameters can be tracked historically? SFNR Value, RDC, SNR, Fluct, Drift?

from __future__ import division, print_function, absolute_import
import sys
import os
import tempfile
import subprocess
import shutil
import getopt
import signal
from glob import glob
from os.path import join, exists, basename, isfile
import re

# for parsing of bxh and results xml files
import xml.dom.minidom

# saving of analysis results
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

import datetime as dt

# packing/unpacking results directory to blob for database
from io import BytesIO
import tarfile

# Qt user interface
from qtpy.QtWidgets import (
    QLabel, QSlider, QHBoxLayout,
    QVBoxLayout, QApplication, QWidget, QStyle, QMessageBox, QFileDialog,
    QDialog, QComboBox, QDialogButtonBox, QGridLayout, QInputDialog,
    QMainWindow, QAction, QGroupBox, QSpinBox, QFormLayout
)
from qtpy.QtGui import QCursor, QIcon
from qtpy.QtWebEngineWidgets import QWebEngineView as QWebView
from qtpy.QtCore import Qt, QThread, QUrl, QTimer
from qtpy.compat import getexistingdirectory

# dicom access - nb expect dcmfetch to use pyqt as well
from dcmfetch.fetchdialog import FetchDialog


# Python 2/3 specific
if sys.version_info > (3, 0):
    # for asynchronous reading of cmd output
    from queue import Queue
    unicode = str
else:
    from Queue import Queue

# NB additional dependency for python < 2.7
try:
    from collections import OrderedDict
except ImportError:
    from ordereddict import OrderedDict


# BEGIN-VERSION-IMPORT
try:
    from . version import __version__
except (ValueError, ImportError):
    pass
# END-VERSION-IMPORT
# MERGE-VERSION-HERE


class AsyncFileReader(QThread):
    """
    Asynchronous reading of a file in a separate thread.

    Pushes read lines on a queue to be consumed in another thread.
    """

    def __init__(self, fd, queue):
        QThread.__init__(self)
        self._fd = fd
        self._queue = queue

    def run(self):
        """Read lines and put them on the queue."""
        for line in iter(self._fd.readline, ''):
            self._queue.put(line)
            self.msleep(10)
        ''' NB: Seems to go in fits and starts if we iterate directly...
        for line in self._fd:
            self._queue.put(line)
            self.msleep(10)
        '''

    def eof(self):
        """Check whether finished."""
        return self.isFinished() and self._queue.empty()


def parse_bxh(bxhfile):
    """
    Extract scan parameters from bxh xml file.

    Currently only the dimensions of the data.
    Returns a dictionary.
    """
    bxh_xml = xml.dom.minidom.parse(bxhfile)
    dim_nodes = bxh_xml.getElementsByTagName("dimension")

    # Check for mosaic series (has no z but a z-split2 instead)
    mosaic = (
        not bool([n for n in dim_nodes if n.getAttribute("type") == 'z']) and
        bool([n for n in dim_nodes if n.getAttribute("type") == 'z-split2'])
    )

    # Extract data dimensions
    params = {}
    for node in dim_nodes:
        if node.getAttribute("type") == 'x':
            size_node = node.getElementsByTagName("size")[0]
            params['nx'] = int(size_node.childNodes[0].data)
        if node.getAttribute("type") == 'y':
            size_node = node.getElementsByTagName("size")[0]
            params['ny'] = int(size_node.childNodes[0].data)
        if node.getAttribute("type") == 't':
            size_node = node.getElementsByTagName("size")[0]
            params['nt'] = int(size_node.childNodes[0].data)
        # The 'z' dim seems problematic for Siemens Mosaic images.
        # There seem to be two z sections z-split1 and z-split2
        # both with the wrong count (6)
        # There is a list of slices though so we'll use that.
        # For multiframe Philips and single frame GE z seems OK.
        if mosaic:
            if node.getAttribute("type") == 'z-split2':
                params['nz'] = len(
                    node.getAttribute("outputselect").split(' ')
                )
        else:
            if node.getAttribute("type") == 'z':
                size_node = node.getElementsByTagName("size")[0]
                params['nz'] = int(size_node.childNodes[0].data)

    return params


def parse_xml_results(resultsfile):
    """
    Parse xml results file ('QASummary.xml').

    Returns a dictionary.
    NB. Not all the fields are parsed out.
    """
    params = {}
    params['signal'] = []
    params['spectrum'] = []
    params['relstdmeas'] = []
    params['relstdcalc'] = []
    params['fwhm'] = []
    params['ghostiness'] = []
    params['cmass'] = []
    qa_xml = xml.dom.minidom.parse(resultsfile)
    for n0 in qa_xml.getElementsByTagName("measurementGroup"):
        for n1 in n0.getElementsByTagName("entity"):
            for n2 in n1.getElementsByTagName("label"):
                if n2.getAttribute("termID") == "summarystats":
                    for obs in n0.getElementsByTagName("observation"):
                        name = obs.getAttribute("name")
                        type_ = obs.getAttribute("type")
                        # handle empty fields (no children)
                        try:
                            data = obs.childNodes[0].data
                            if type_ == 'integer':
                                value = int(data)
                            elif type_ == 'float':
                                value = float(data)
                            elif type_ == 'varchar':
                                value = str(data)
                        except (IndexError, ValueError):
                            value = None
                        params[name] = value
                elif n2.getAttribute("termID") == "signal":
                    for obs in n0.getElementsByTagName("observation"):
                        if obs.getAttribute("name") == 'frameNum':
                            framenum = int(obs.childNodes[0].data)
                        elif obs.getAttribute("name") == 'rawSignalROI':
                            rawsignalroi = float(obs.childNodes[0].data)
                        elif obs.getAttribute("name") == 'rawSignalFit':
                            rawsignalfit = float(obs.childNodes[0].data)
                    params['signal'].append(
                        (framenum, rawsignalroi, rawsignalfit)
                    )
                elif n2.getAttribute("termID") == "spectrum":
                    for obs in n0.getElementsByTagName("observation"):
                        if obs.getAttribute("name") == 'frequency':
                            frequency = float(obs.childNodes[0].data)
                        elif obs.getAttribute("name") == 'spectrum':
                            spectrum = float(obs.childNodes[0].data)
                    params['spectrum'].append((frequency, spectrum))
                elif n2.getAttribute("termID") == "relstdmeas":
                    for obs in n0.getElementsByTagName("observation"):
                        if obs.getAttribute("name") == 'roiWidth':
                            roiwidth = int(obs.childNodes[0].data)
                        elif obs.getAttribute("name") == 'relSTDMeas':
                            relstdmeas = float(obs.childNodes[0].data)
                    params['relstdmeas'].append((roiwidth, relstdmeas))
                elif n2.getAttribute("termID") == "relstdcalc":
                    for obs in n0.getElementsByTagName("observation"):
                        if obs.getAttribute("name") == 'roiWidth':
                            roiwidth = int(obs.childNodes[0].data)
                        elif obs.getAttribute("name") == 'relSTDCalc':
                            relstdcalc = float(obs.childNodes[0].data)
                    params['relstdcalc'].append((roiwidth, relstdcalc))
                elif n2.getAttribute("termID") == "FWHM":
                    for obs in n0.getElementsByTagName("observation"):
                        if obs.getAttribute("name") == 'volnum':
                            volnum = int(obs.childNodes[0].data)
                        elif obs.getAttribute("name") == 'FWHMX':
                            fwhmx = float(obs.childNodes[0].data)
                        elif obs.getAttribute("name") == 'FWHMY':
                            fwhmy = float(obs.childNodes[0].data)
                        elif obs.getAttribute("name") == 'FWHMZ':
                            fwhmz = float(obs.childNodes[0].data)
                    params['fwhm'].append((volnum, fwhmx, fwhmy, fwhmz))
                elif n2.getAttribute("termID") == "ghostiness":
                    for obs in n0.getElementsByTagName("observation"):
                        if obs.getAttribute("name") == 'volNum':
                            volnum = int(obs.childNodes[0].data)
                        elif obs.getAttribute("name") == 'ghostPercent':
                            ghostpercent = float(obs.childNodes[0].data)
                        elif obs.getAttribute("name") == 'brightGhostPercent':
                            brightghostpercent = float(obs.childNodes[0].data)
                    params['ghostiness'].append(
                        (volnum, ghostpercent, brightghostpercent)
                    )
                elif n2.getAttribute("termID") == "cmass":
                    for obs in n0.getElementsByTagName("observation"):
                        if obs.getAttribute("name") == 'volnum':
                            volnum = int(obs.childNodes[0].data)
                        elif obs.getAttribute("name") == 'cmassx':
                            cmassx = float(obs.childNodes[0].data)
                        elif obs.getAttribute("name") == 'cmassy':
                            cmassy = float(obs.childNodes[0].data)
                        elif obs.getAttribute("name") == 'cmassz':
                            cmassz = float(obs.childNodes[0].data)
                    params['cmass'].append((volnum, cmassx, cmassy, cmassz))

    return params


class SliderCombo(QWidget):
    """A combination widget with a linked slider and spin box."""

    def __init__(self, label='', range_=(1, 10), value=5, tracking=True, stepping=1, parent=None):
        super(SliderCombo, self).__init__(parent)

        slider_label = QLabel(label + ':')
        self.slider = QSlider(Qt.Horizontal)
        self.slider.setRange(*range_)
        self.slider.setValue(value)
        self.slider.setTracking(tracking)
        self.slider.setSingleStep(stepping)

        self.spinbox = QSpinBox()
        self.spinbox.setMinimum(range_[0])
        self.spinbox.setMaximum(range_[1])
        self.spinbox.setValue(value)
        self.spinbox.setSingleStep(stepping)

        self.slider.valueChanged.connect(self.spinbox.setValue)
        self.spinbox.valueChanged.connect(self.slider.setValue)

        layout = QHBoxLayout()
        layout.addWidget(slider_label)
        layout.addWidget(self.slider)
        layout.addWidget(self.spinbox)
        self.setLayout(layout)

    def setValue(self, newvalue):
        """Set value of slider/spinbox."""
        self.slider.setValue(newvalue)

    def setSingleStep(self, step):
        """Set the Stepping of the slider/spinbox."""
        self.slider.setSingleStep(step)
        self.spinbox.setSingleStep(step)

    def value(self):
        """Return current value of slider/spinbox."""
        return self.slider.value()

    def setRange(self, range_):
        """
        Set slider/spinbox range.

        Takes tuple (lower, upper)
        """
        self.slider.setRange(*range_)
        self.spinbox.setMinimum(range_[0])
        self.spinbox.setMaximum(range_[1])
        self.slider.setValue(sum(range_) // 2)


class ControlPanel(QWidget):
    """
    Area to display information about the scan.

    Allows selection of options for the analysis including choice of
    region of interest to extract for the plots.
    """

    def __init__(self, parent=None):
        """Set layout of panel in constructor."""
        super(ControlPanel, self).__init__(parent)

        self.options_group = QGroupBox('Options')

        self.zpos = SliderCombo(
            label='Z Position', range_=(1, 32), value=13,
            tracking=True, stepping=1,
            parent=self
        )
        self.tskip = SliderCombo(
            label='Initial Frames', range_=(1, 10), value=2,
            tracking=True, stepping=2,
            parent=self
        )

        # ROI size - dflt as per Friedman&Glover ie 15 for image 64*64 or less
        self.roisize = SliderCombo(
            label='ROI Size', range_=(10, 40), value=15,
            tracking=True, stepping=1,
            parent=self
        )

        analysis_vllayout = QVBoxLayout()
        for w in [self.zpos, self.tskip, self.roisize]:
            analysis_vllayout.addWidget(w)
        analysis_vllayout.addStretch()

        analysis_vrlayout = QVBoxLayout()
        analysis_vrlayout.addStretch()

        analysis_hlayout = QHBoxLayout()
        analysis_hlayout.addLayout(analysis_vllayout)
        analysis_hlayout.addLayout(analysis_vrlayout)
        analysis_hlayout.addStretch()

        self.options_group.setLayout(analysis_hlayout)

        layout = QVBoxLayout()
        layout.addWidget(self.options_group)

        # The results of the fBIRN analysis
        self.field_dict = dict()

        self.results_group = QGroupBox('Results')
        results_layout = QHBoxLayout()

        widgets = self._create_text_fields(self._analysis_results_fields(None))
        self.field_dict.update(
            self._add_fields_to_layout(widgets, results_layout)
        )
        self.results_group.setLayout(results_layout)
        layout.addWidget(self.results_group)

        self.setLayout(layout)

    def _create_text_fields(self, fieldlist):
        """
        Create the label widgets for the information text fields.

        Required names and initial (string) values passed as list of 2-tuples.
        """
        widgets = []
        for field_row in fieldlist:
            widget_row = []
            for fieldname, fieldvalue in field_row:
                f = QLabel(fieldvalue)
                widget_row += [(fieldname, f)]
            widgets.append(widget_row)
        return widgets

    def _add_fields_to_layout(self, field_and_widget_list, layout):
        """
        Add the information widgets to an hbox layout.

        Uses a set of QFormlayouts so each fields is cleanly associated
        with its title text.
        """
        field_dict = {}
        ncols = max([len(l) for l in field_and_widget_list])
        form_layouts = [QFormLayout() for i in range(ncols)]
        for form_layout in form_layouts:
            layout.addLayout(form_layout)

        for row_list in field_and_widget_list:
            for col, (field_name, field_widget) in enumerate(row_list):
                form_layouts[col].addRow(
                    '<em>' + field_name + ':</em>', field_widget
                )
                field_dict[field_name] = field_widget
            for col in range(len(row_list), ncols):
                form_layouts[col].addRow('', QLabel(''))

        return field_dict

    def _analysis_results_fields(self, results):
        """
        Fields to display from fBIRN analysis.

        The layout is a list of lists. Form is so as to correspond
        to the order of the information in the html plots.
        """
        fields = []
        for line in [
            [("Fluct(%)", "percentFluc"), ("Drift", "drift"), ("Drift Fit", "driftfit")],
            [("Mean Signal", "mean"),     ("SNR", "SNR"),     ("SFNR", "SFNR")],
            [("RDC", "rdc"),              ("Std Dev", "std")],
            [("Min FWHM X", "minFWHMX"),  ("Mean FWHM X", "meanFWHMX"), ("Max FWHM X", "maxFWHMX")],
            [("Min FWHM Y", "minFWHMY"),  ("Mean FWHM Y", "meanFWHMY"), ("Max FWHM Y", "maxFWHMY")],
            [("Min FWHM Z", "minFWHMZ"),  ("Mean FWHM Z", "meanFWHMZ"), ("Max FWHM Z", "maxFWHMZ")],
            [("Disp. X", "dispCMassX"),   ("Drift X", "driftCMassX")],
            [("Disp. Y", "dispCMassY"),   ("Drift Y", "driftCMassY")],
            [("Disp. Z", "dispCMassZ"),   ("Drift Z", "driftCMassZ")],
            [("Mean Ghosts (%)", "meanGhost"),   ("Bright Ghosts (%)", "meanBrightGhost")]
        ]:
            fields_row = []
            for label, tag in line:
                try:
                    fields_row.append((label, results[tag]))
                except TypeError:
                    fields_row.append((label, ''))
                except KeyError:
                    fields_row.append((label, '---'))
            fields.append(fields_row)
        return fields

    def configure(self, zconf, tconf, rconf):
        """
        Configure the sliders for the analysis options.

        We expect each passed param to be a tuple (min, max, default, skip=1)
        """
        self.zpos.setRange(zconf[0:2])
        self.tskip.setRange(tconf[0:2])
        self.roisize.setRange(rconf[0:2])
        self.zpos.setValue(zconf[2])
        self.tskip.setValue(tconf[2])
        self.roisize.setValue(rconf[2])
        self.tskip.setSingleStep(2)

    def update_results(self, results):
        """Update text in the results fields on completion of the analysis."""
        fields = [
            item
            for row in self._analysis_results_fields(results)
            for item in row
        ]
        for tag, val in fields:
            self.field_dict[tag].setText(str(val))

    def options(self):
        """A dict describing the current state of the analysis options."""
        return {
            'zpos': self.zpos.value(),
            'tskip': self.tskip.value(),
            'roisize': self.roisize.value()
        }


class ReviewDialog(QDialog):
    """Dialog to select a previous day's analysis to display."""

    DATEFORMAT = '%A %d/%b/%Y'

    def __init__(self, datelist, scanner='', parent=None):
        """Initialize with a list of datetime objects."""
        super(ReviewDialog, self).__init__(parent)
        if scanner:
            self.setWindowTitle(scanner)

        label = QLabel('QA Scan Date:')
        combobox = QComboBox()
        label.setBuddy(combobox)
        combobox.addItems([
            scandate.strftime(self.DATEFORMAT)
            for scandate in sorted(datelist, reverse=True)
        ])

        buttonbox = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel
        )
        buttonbox.button(QDialogButtonBox.Ok).setDefault(True)

        buttonbox.accepted.connect(self.accept)
        buttonbox.rejected.connect(self.reject)

        layout = QGridLayout()
        layout.addWidget(label, 0, 0)
        layout.addWidget(combobox, 0, 1, 1, 2)
        layout.addWidget(buttonbox, 1, 0, 1, 3)

        self.setLayout(layout)
        self._combobox = combobox

    def get_date(self):
        """Return the selected date as a datetime object."""
        return dt.datetime.strptime(
            str(self._combobox.currentText()), self.DATEFORMAT
        )


class FBirnMainWindow(QMainWindow):
    """
    The application main window.

    Consists of a central widget with a control panel and
    an html viewer and a tool bar, menus and a footer.
    """

    DBNAME = 'fMRIQAResults'
    DBHOST = 'canopus'
    SCANNER = 'CRIC45064'
    PREFIX = 'QA'
    DICOM2BXH = '/usr/local/bxh/bin/dicom2bxh'
    QAPERLSCRIPT = '/usr/local/bxh/bin/fmriqa_phantomqa.pl'

    FIELDS = [
        "percentFluc", "drift",     "driftfit",
        "mean",        "SNR",       "SFNR",
        "rdc",         "std",
        "minFWHMX",    "meanFWHMX", "maxFWHMX",
        "minFWHMY",    "meanFWHMY", "maxFWHMY",
        "minFWHMZ",    "meanFWHMZ", "maxFWHMZ",
        "dispCMassX",  "driftCMassX",
        "dispCMassY",  "driftCMassY",
        "dispCMassZ",  "driftCMassZ",
        "meanGhost",   "meanBrightGhost",
        "scandate",    "operator",
        "tskip",       "zpos",      "roisize"
    ]

    FIELD_TYPES = [
        "double precision", "double precision", "double precision",
        "double precision", "double precision", "double precision",
        "double precision", "double precision",
        "double precision", "double precision", "double precision",
        "double precision", "double precision", "double precision",
        "double precision", "double precision", "double precision",
        "double precision", "double precision",
        "double precision", "double precision",
        "double precision", "double precision",
        "double precision", "double precision",
        "date", "text",
        "integer", "integer", "integer"
    ]

    def __init__(self, imagedir=None, parent=None):
        """Optionally specify a directory with images."""
        super(FBirnMainWindow, self).__init__(parent)

        self.imagedir = imagedir
        self.wrapped = None
        self.qadir = None
        self.scanners = self._scannernames()
        if self.scanners:
            if self.SCANNER in self.scanners:
                self.current_scanner = self.SCANNER
            else:
                self.current_scanner = self.scanners[0]
        else:
            self.current_scanner = None

        self.setWindowTitle('FBIRN fMRI QA Analysis')

        file_actions = self._create_file_actions()
        analysis_actions = self._create_analysis_actions()
        tools_actions = self._create_tools_actions()
        help_actions = self._create_help_actions()

        menunames_and_actions = [
            ("&File", file_actions),
            ("&Analysis", analysis_actions),
            ("&Tools", tools_actions),
            ("&Help", help_actions)
        ]
        menus = []
        for menuname, actionsdict in menunames_and_actions:
            menu = self.menuBar().addMenu(menuname)
            self._add_actions(menu, actionsdict.values())
            menus.append(menu)

        toolbarnames_and_actions = [
            ("File", file_actions),
            ("Analysis", analysis_actions),
        ]
        toolbars = []
        for toolbarname, actionsdict in toolbarnames_and_actions:
            toolbar = self.addToolBar(toolbarname)
            self._add_actions(toolbar, actionsdict.values())
            toolbars.append(toolbar)

        # Keep all the actions so we can enable/disable them later
        self.actions_dict = {}
        for actions in [file_actions, analysis_actions, help_actions]:
            self.actions_dict.update(actions)
        self.status_bar = self._create_status_bar()

        central_widget = self._create_central_widget()
        self.setCentralWidget(central_widget)
        central_widget.setEnabled(False)

        if self.imagedir is not None:
            try:
                self.wrapped = '.'.join([self.imagedir, 'xml'])
                self.qadir = '-'.join([self.imagedir, 'QA'])
                self.convert_to_bxh(self.imagedir, self.wrapped)
                self.run_analysis(self.wrapped, self.qadir)
                # shutil.rmtree(wrapped, ignore_errors=True)
            except IOError as e:
                QMessageBox.warning(
                    self, 'Unable to load series from %s (%s)' % (imagedir, e)
                )

    def _create_central_widget(self):
        """
        Create Central Widget.

        The central widget will consist of a control and info panel on the left
        and an html viewer on the right.
        """
        main_frame = QWidget(self)

        self.control_panel = ControlPanel()
        self.results_view = QWebView(parent=self)

        vbox = QVBoxLayout()
        vbox.addWidget(self.control_panel)
        vbox.addStretch()
        hbox = QHBoxLayout()
        hbox.addLayout(vbox)
        hbox.addStretch()
        hbox.addWidget(self.results_view)
        main_frame.setLayout(hbox)

        return main_frame

    def _create_status_bar(self):
        """Add a status bar to the main window."""
        status_text = QLabel("FBIRN Analysis")
        self.statusBar().addWidget(status_text, 1)
        return status_text

    def _create_file_actions(self):
        """Create actions to be placed under the 'File' menu."""
        fetch_file_action = self._create_action(
            "&Fetch Series", shortcut="Ctrl+F", slot=self.fetch_series,
            icon='download', tip="Fetch new DICOM Series from Server"
        )

        load_file_action = self._create_action(
            "&Load Series", shortcut="Ctrl+L", slot=self.load_series,
            icon=QStyle.SP_DirOpenIcon, tip="Load new DICOM Series from Disc"
        )

        quit_action = self._create_action(
            "&Quit", slot=self.close, icon="filequit",
            shortcut="Ctrl+Q", tip="Close the application"
        )

        return OrderedDict([
            ('File:Fetch', fetch_file_action),
            ('File:Load', load_file_action),
            ('File:Quit', quit_action)
        ])

    def _create_analysis_actions(self):
        """Create actions to be placed under the 'Analysis' menu."""
        analyse_action = self._create_action(
            "A&nalyse", shortcut="Ctrl+A", slot=self.run_analysis,
            icon="fbirn", tip="Perform fBIRN phantom QA analysis",
            enabled=False
        )

        save_action = self._create_action(
            "&Save Results", shortcut="Ctrl+S", slot=self.save_results,
            icon="filesave", tip="Save the results of analyses",
            enabled=False
        )

        review_action = self._create_action(
            "R&eview", shortcut="Ctrl+R", slot=self.review, icon="review",
            tip="Review previous phantom QA analysis",
            enabled=True
        )

        return OrderedDict([
            ('Analysis:Analyse', analyse_action),
            ('Analysis:Save', save_action),
            ('Analysis:Review', review_action)
        ])

    def _create_tools_actions(self):
        """Create actions to be placed under the 'Tools' menu."""
        new_db_action = self._create_action(
            "New D&atabase", shortcut="Ctrl+D", slot=self.new_db,
            icon="newdb", tip="Create new results database",
            enabled=True
        )

        new_scanner_action = self._create_action(
            "&New Scanner", shortcut="Ctrl+N", slot=self.new_scanner,
            icon="newtable", tip="Create db table for new scanner",
            enabled=True
        )

        change_scanner_action = self._create_action(
            "&Change Scanner", shortcut="Ctrl+C", slot=self.change_scanner,
            icon="scanner", tip="Change scanner",
            enabled=True
        )

        return OrderedDict([
            ('Tools:NewDB', new_db_action),
            ('Tools:NewScanner', new_scanner_action),
            ('Tools:ChangeScanner', change_scanner_action)
        ])

    def _create_help_actions(self):
        """Create actions to be placed under the 'Help' menu."""
        about_action = self._create_action(
            "&About", shortcut='F1', slot=self.on_about, icon="about",
            tip='About the demo'
        )
        return OrderedDict([
            ('Help:About', about_action)
        ])

    def _add_actions(self, target, actions):
        """
        Add all the actions specified in the actions list to target.

        Target may be either a menu or a button bar
        """
        for action in actions:
            if action is None:
                target.addSeparator()
            else:
                target.addAction(action)

    def _create_action(self, text, slot=None, shortcut=None,
                       icon=None, tip=None, checkable=False,
                       signal="triggered", enabled=True):
        """
        Create an Action.

        Convenience function for creating an action based on the
        given parameters with defaults for most things.
        """
        action = QAction(text, self)
        if type(icon) is QStyle.StandardPixmap:
            style = QApplication.style()
            action.setIcon(QIcon(style.standardIcon(icon)))
        elif type(icon) in [str, unicode]:
            # Specified with a colon: uses precompiled resources
            action.setIcon(QIcon(":images/%s.png" % icon))

        if shortcut is not None:
            action.setShortcut(shortcut)
        if tip is not None:
            action.setToolTip(tip)
            action.setStatusTip(tip)
        if slot is not None:
            getattr(action, signal).connect(slot)
        if checkable:
            action.setCheckable(True)
        action.setEnabled(enabled)
        return action

    def fetch_series(self):
        """Fetch dicom series from DICOM server on the network."""
        try:
            dialog = FetchDialog(parent=self, multiple_selection=False)
            if dialog.exec_() and dialog.image_directories and dialog.image_files:
                imagedir = dialog.image_directories[0]
                wrapped = '.'.join([imagedir, 'xml'])

                self.convert_to_bxh(imagedir, wrapped)

                params = parse_bxh(wrapped)
                # must be allways such that no of point is even otherwise falls over
                tconf = (
                    0 if params['nt'] % 2 == 0 else 1,
                    params['nt'] if params['nt'] % 2 == 0 else params['nt'] - 1,
                    2 if params['nt'] % 2 == 0 else 3
                )
                self.control_panel.configure(
                    zconf=(1, params['nz'], params['nz'] // 2),
                    tconf=tconf,
                    rconf=(10, 40, 30 if params['nx'] > 64 else 15)
                )

                if self.imagedir is not None:
                    shutil.rmtree(self.imagedir, ignore_errors=True)
                if self.wrapped is not None:
                    shutil.rmtree(self.wrapped, ignore_errors=True)

                self.imagedir = imagedir
                self.wrapped = wrapped
                for key in ['Analysis:Analyse']:
                    self.actions_dict[key].setEnabled(True)
                self.centralWidget().setEnabled(True)
                self.control_panel.options_group.setEnabled(True)
                self.control_panel.results_group.setEnabled(False)

        except IOError as e:
            QMessageBox.warning(
                self, 'Unable to fetch dicom series', unicode(e)
            )
        except subprocess.CalledProcessError as e:
            QMessageBox.warning(
                self, 'Unable to run convert series:', unicode(e)
            )
        except OSError as e:
            QMessageBox.warning(
                self, 'Error processing series:', unicode(e)
            )
        except ValueError as e:
            QMessageBox.warning(
                self, 'Dialog error:', unicode(e)
            )
        finally:
            # dialog.free_image_files()
            # should keep these until we load a new series.
            pass

    def load_series(self):
        """
        Load a dicom series from a directory.

        Expects a series of dicom objects.
        """
        try:
            # The 'DontUseNative' flag is needed as the native dialog is broken
            flags = (
                QFileDialog.DontUseNativeDialog |
                QFileDialog.ReadOnly |
                QFileDialog.ShowDirsOnly
            )
            srcimagedir = getexistingdirectory(
                self, "Load Dicom Files", os.getcwd(),
                flags
            )
            if not srcimagedir:
                return
            if not exists(srcimagedir):
                QMessageBox.warning(
                    self, "Unable to load dicom series",
                    "Directory '%s' does not exist" % srcimagedir
                )
                return

            # Take a temp copy: a little dubious but races are unlikely here
            imagedir = tempfile.mkdtemp(prefix='fbirn-load')
            os.rmdir(imagedir)
            shutil.copytree(srcimagedir, imagedir)
            wrapped = '.'.join([imagedir, 'xml'])

            self.convert_to_bxh(
                imagedir, wrapped, patterns=[r'[0-9]*', r'*.dcm']
            )
            params = parse_bxh(wrapped)

            self.control_panel.configure(
                zconf=(1, params['nz'], params['nz'] // 2),
                tconf=(1, params['nt'], 2 if params['nt'] % 2 == 0 else 3),
                rconf=(10, 40, 30 if params['nx'] > 64 else 15))
            if self.imagedir is not None:
                shutil.rmtree(self.imagedir, ignore_errors=True)
            if self.wrapped is not None:
                shutil.rmtree(self.wrapped, ignore_errors=True)

            self.imagedir = imagedir
            self.wrapped = wrapped
            for key in ['Analysis:Analyse']:
                self.actions_dict[key].setEnabled(True)
            self.centralWidget().setEnabled(True)
            self.control_panel.results_group.setEnabled(False)
        except IOError as e:
            QMessageBox.warning(
                self, 'Unable to load dicom series', unicode(e)
            )
        except subprocess.CalledProcessError as e:
            QMessageBox.warning(
                self, 'Unable to run convert series:', unicode(e)
            )
        except OSError as e:
            QMessageBox.warning(
                self, 'Error processing series:', unicode(e)
            )
        return

    def _dbtable_to_scanner(self, dbtable):
        """Get scanner corresponding to database table."""
        if dbtable:
            return dbtable[len(self.PREFIX):].upper()
        else:
            return dbtable

    def _scanner_to_dbtable(self, scanner):
        """Get Database table corresponding to scanner."""
        # Avoid problematic characters like '-'
        if scanner:
            cleaned = re.sub(r'\W', '_', scanner)
            return (self.PREFIX + cleaned).lower()
        else:
            return scanner

    def _scanner_sanitised(self, scanner):
        """
        Get sanitised version of scanner name

        As would be returned by _dbtable_to_scanner
        """
        if scanner:
            return re.sub(r'\W', '_', scanner).upper()
        else:
            return scanner

    def run_analysis(self):
        """
        Run the fBIRN QA analysis.

        Expects a bxh xml file decscribing
        the data series. Produces an xml file 'summaryQA.xml' and a
        collection of html and pngs for the web report.
        We parse the xml file to populate the info panel. The command
        will run in the background but we'll read the output line by line
        updating the user interface so will only return on completion.
        """
        self.qadir = '-'.join([self.imagedir, 'QA'])
        if exists(self.qadir):
            shutil.move(self.qadir, self.qadir + '_backup')

        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
        try:
            indir = self.wrapped
            outdir = self.qadir
            options = self.control_panel.options()
            cmdline = [
                self.QAPERLSCRIPT,
                '--timeselect', str(options['tskip']) + ':',
                '--zselect', str(options['zpos']),
                '--roisize', str(options['roisize']),
                indir, outdir
            ]

            # Command will run in the background and we'll read the output
            process = subprocess.Popen(
                cmdline, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                universal_newlines=True
            )
            stdout_queue = Queue()
            stdout_reader = AsyncFileReader(process.stdout, stdout_queue)
            stdout_reader.start()

            # Get lines of output from command
            while not stdout_reader.eof():
                line = None
                while not stdout_queue.empty():
                    line = stdout_queue.get()
                if line is not None:
                    self.status_bar.setText(str(line))
                # Wake up the UI
                QApplication.processEvents()
                QThread.msleep(50)

            # Join threads we've started and close subprocess file descriptors.
            stdout_reader.wait()
            process.stdout.close()
            self.status_bar.setText('Done')
            # The results as xml and as a web page
            xml_file = join(outdir, 'summaryQA.xml')
            html_file = join(outdir, 'index.html')
            try:
                self.results = parse_xml_results(xml_file)
                # record the options used as well
                self.results.update(options)
                self.control_panel.update_results(self.results)

                self.results_view.load(QUrl.fromLocalFile(html_file))

                for key in ['Analysis:Save']:
                    self.actions_dict[key].setEnabled(True)
                self.control_panel.results_group.setEnabled(True)
            except IOError as e:
                QMessageBox.warning(
                    self, 'Unable to find results of analysis:', unicode(e)
                )
            if exists(self.qadir + '_backup'):
                shutil.rmtree(self.qadir + '_backup')
        except subprocess.CalledProcessError as e:
            QMessageBox.warning(self, 'Unable to run analysis:', unicode(e))
            if exists(self.qadir + '_backup'):
                shutil.rmtree(self.qadir)
                shutil.move(self.qadir + '_backup', self.qadir)
        finally:
            QApplication.restoreOverrideCursor()
        return

    def save_results(self):
        """
        Export results to a database.

        Summary info and a blob for a tar of the results directory.
        """
        # check current tablename: want it to match the node name if possible
        expected_scanner = self._scanner_sanitised(self.results['scanner'])
        if expected_scanner in self.scanners:
            if expected_scanner != self.current_scanner:
                reply = QMessageBox.question(
                    self,
                    'Results are not for %s' % self.current_scanner,
                    'Change current scanner to %s and save?' % expected_scanner,
                    QMessageBox.Yes | QMessageBox.No,
                    QMessageBox.No
                )
                if reply != QMessageBox.Yes:
                    return
        else:
            reply = QMessageBox.question(
                self,
                'No database table for %s' % expected_scanner,
                'Create a new table %s?' % self._scanner_to_dbtable(expected_scanner),
                QMessageBox.Yes | QMessageBox.No,
                QMessageBox.No
            )
            if reply != QMessageBox.Yes:
                return
            self.new_scanner(expected_scanner)
            self.scanners = self._scannernames()
            if expected_scanner not in self.scanners:
                QMessageBox.warning(
                    self,
                    'Unable to save results:',
                    'Cant find db table for %s' % expected_scanner
                )
                return

        self.current_scanner = expected_scanner

        dbtable = self._scanner_to_dbtable(self.current_scanner)

        results_to_save = dict(
            (k, v) for k, v in self.results.items() if k in self.FIELDS
        )
        if self.results:
            connection = None
            try:
                connection = psycopg2.connect(
                    database=self.DBNAME, host=self.DBHOST
                )
                cursor = connection.cursor()
                cursor.execute(
                    'SELECT scandate FROM ' + dbtable + ' WHERE scandate = %s',
                    [results_to_save['scandate']]
                )
                if len(list(cursor)) > 0:
                    reply = QMessageBox.question(
                        self, "Save Results",
                        "Results for '%s' already exists - overwrite?" % results_to_save['scandate'],
                        QMessageBox.Yes, QMessageBox.No
                    )
                    if reply == QMessageBox.No:
                        # NB: it's ok, this *does* leave via the finally block
                        return

                # Add a key for a blob with a tar of the entire qa directory
                # This is about 1MByte uncompressed
                results_to_save['qablob'] = psycopg2.Binary(
                    self.tar_blob(self.qadir)
                )

                # Update/Insert all the (key, value) pairs into the database.
                sql = 'UPDATE ' + dbtable
                sql += ' SET '
                sql += ', '.join(
                    '%s=%%(%s)s' % (k, k) for k in results_to_save
                )
                sql += ' WHERE scandate=%(scandate)s'
                sql += ';'

                cursor.execute(sql, results_to_save)
                if cursor.rowcount < 1:
                    sql = 'INSERT INTO ' + dbtable
                    sql += ' ('
                    sql += ', '.join(results_to_save)
                    sql += ') VALUES ('
                    # NB this syntax is dependent on the database back end
                    sql += ', '.join(
                        '%(' + str(k) + ')s' for k in results_to_save
                    )
                    sql += ');'

                    cursor.execute(sql, results_to_save)
                connection.commit()
                cursor.close()
                QMessageBox.information(
                    self, "Save Results",
                    "Results for %s on %s saved to database"
                    % (results_to_save['scandate'], self.current_scanner)
                )
            except psycopg2.OperationalError as e:
                QMessageBox.critical(
                    self, "Save Results",
                    "Can't save - problem with database %s: %s" % (self.DBNAME, e)
                )
            finally:
                if connection is not None:
                    connection.close()

    def review(self):
        """
        Review a previous QA analysis.

        Prompts for the date of the QA analysis(the scan date) with
        a dialog listing those in the database.
        """
        dates = self.list_qadates()
        if not dates:
            QMessageBox.information(
                self, "Review",
                "No previous result available for %s" % self.current_scanner
            )
            return
        dialog = ReviewDialog(dates, self.current_scanner)
        if dialog.exec_():
            qadate = dialog.get_date()
        else:
            return

        if self.imagedir is not None:
            shutil.rmtree(self.imagedir, ignore_errors=True)
        if self.wrapped is not None:
            shutil.rmtree(self.wrapped, ignore_errors=True)
        if self.qadir is not None:
            shutil.rmtree(self.qadir, ignore_errors=True)

        self.qadir = tempfile.mkdtemp(prefix='fbirn-review')
        try:
            blob = self.get_blob(qadate)
            self.untar_blob(blob, self.qadir)

            # The results as xml and as a web page
            xml_file = join(self.qadir, 'summaryQA.xml')
            html_file = join(self.qadir, 'index.html')
            try:
                self.results = parse_xml_results(xml_file)
                self.control_panel.update_results(self.results)
                self.results_view.load(QUrl.fromLocalFile(html_file))
                self.centralWidget().setEnabled(True)
                for key in ['Analysis:Save']:
                    self.actions_dict[key].setEnabled(False)
                for key in ['Analysis:Analyse']:
                    self.actions_dict[key].setEnabled(False)
                # oops - don't really have enough info here to set the ranges
                # we'll just put in something plausible
                # nb timepoints has a trailing ':'
                zpos = int(self.results['slice'])
                tskip = int(self.results['timepoints'][:-1])
                roisize = int(self.results['roiSize'])
                self.control_panel.configure(
                    zconf=(1, 30, zpos),
                    tconf=(1, 20, tskip),
                    rconf=(10, 40, roisize)
                )
                self.control_panel.options_group.setEnabled(False)
                self.control_panel.results_group.setEnabled(True)

            except IOError as e:
                QMessageBox.warning(
                    self, 'Unable to find results of analysis:', unicode(e)
                )
            if exists(self.qadir + '_backup'):
                shutil.rmtree(self.qadir + '_backup')
        except (psycopg2.Error, OSError) as e:
            QMessageBox.warning(
                self, 'Unable to extract previous results:', unicode(e)
            )
        return

    def _scannernames(self):
        """
        Retrieve a list of scanners.

        There is a database table for each one which is called by the system
        name with a prefix to avoid clashes with postgres system tables.
        """
        connection = None
        try:
            connection = psycopg2.connect(
                database=self.DBNAME, host=self.DBHOST
            )
            connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
            cursor = connection.cursor()
            sql = 'SELECT tablename FROM pg_catalog.pg_tables;'
            cursor.execute(sql)
            # strip the prefix
            return [
                self._dbtable_to_scanner(i[0])
                for i in cursor.fetchall()
                if i[0].startswith(self.PREFIX.lower())
            ]
        except (psycopg2.Error, OSError):
            return []
        finally:
            if connection is not None:
                connection.close()

    def change_scanner(self):
        """
        Select the current scanner.

        This determines the database table to use.
        """
        self.scanners = self._scannernames()
        curr_indx = (
            self.scanners.index(self.current_scanner)
            if self.current_scanner in self.scanners
            else 0
        )
        result = QInputDialog.getItem(
            self,
            'Change Scanner',
            'Scanner Name',
            self.scanners,
            curr_indx
        )

        if result and result[1] and result[0]:
            self.current_scanner = result[0]

    def new_scanner(self, scanner=None):
        """Create a database table for the specified scanner."""
        if scanner is None:
            result = QInputDialog.getText(
                self, 'Enter new scanner name', 'Scanner name:'
            )
            if result and result[1] and result[0]:
                scanner = result[0]
            else:
                return
        dbtablename = self._scanner_to_dbtable(scanner)

        connection = None
        try:
            connection = psycopg2.connect(
                database=self.DBNAME, host=self.DBHOST
            )
            connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
            cursor = connection.cursor()
            # Insert all of the dictionary (key, value)'s into the database.
            # The default type info for the columns is NUMERIC. This seems to
            # work OK even though operator and scandate are text.
            # Blob for tar of the analysis directory has to be typed though.
            # Postgres should use 'bytea'

            # hack for 'IF NOT EXISTS' - only in postgres >=9.1
            sql = '''SELECT COUNT(*)
                       FROM  pg_catalog.pg_tables
                          WHERE tablename  = '%s';
                  ''' % dbtablename
            cursor.execute(sql)
            if int(cursor.fetchone()[0]) < 1:
                sql = 'CREATE TABLE ' + dbtablename
                sql += ' (\n '
                sql += ',\n '.join(
                    '%s %s' % (field, type_)
                    for (field, type_) in zip(self.FIELDS, self.FIELD_TYPES)
                )
                sql += ',\n qablob bytea'
                sql += ',\n PRIMARY KEY (scandate)'
                sql += '\n);'
                cursor.execute(sql)
                connection.commit()
                QMessageBox.information(
                    self, "New Table",
                    "Created a new table '%s' for results in %s" % (dbtablename, self.DBNAME)
                )
            else:
                QMessageBox.information(
                    self, "New Table",
                    "Table '%s' already exist in %s" % (dbtablename, self.DBNAME)
                )
                cursor.close()
        except (psycopg2.Error, OSError) as e:
            QMessageBox.critical(
                self, 'New Table',
                "Unable to create new table '%s' in %s: %s" % (dbtablename, self.DBNAME, e)
            )
        finally:
            if connection is not None:
                connection.close()

    def new_db(self):
        """Initialize database.

        For sqlite3 we just need to open the file and
        create the tables. For postgres we would have to create the database
        and create the tables, and would need to sort out permissions.
        """
        connection = None
        try:
            connection = psycopg2.connect(
                database=self.DBNAME, host=self.DBHOST
            )
            db_exists = True
            QMessageBox.information(
                self, "New Database",
                "Database '%s' already exists" % self.DBNAME
            )
        except psycopg2.Error as e:
            db_exists = False
        finally:
            if connection is not None:
                connection.close()

        connection = None
        if not db_exists:
            try:
                connection = psycopg2.connect(
                    database='template1', host=self.DBHOST
                )
                connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
                cursor = connection.cursor()
                cursor.execute('CREATE DATABASE "%s";' % self.DBNAME)
                connection.commit()
                cursor.close()
                QMessageBox.information(
                    self, "New Database",
                    "Created a new database for results '%s'" % self.DBNAME
                )
            except (psycopg2.Error, OSError) as e:
                QMessageBox.critical(
                    self, 'New Database',
                    'Unable to create database "%s": %s' % (self.DBNAME, e)
                )
            finally:
                if connection is not None:
                    connection.close()

        self.new_scanner(self.current_scanner)

    def on_about(self):
        """
        Help about box.

        Shows python, bxh and afni version strings.
        """
        python_version = '.'.join(str(i) for i in sys.version_info[:3])
        try:
            p = subprocess.Popen(
                [self.QAPERLSCRIPT, '--version'],
                stdout=subprocess.PIPE,
                universal_newlines=True
            )
            bxh_version = p.communicate()[0]
            bxh_version = bxh_version[bxh_version.find('BXH'):]
        except (subprocess.CalledProcessError, OSError) as e:
            bxh_version = '<span style="color:red">UNKNOWN(%s)</span>' % e
        try:
            p = subprocess.Popen(
                ['afni', '--version'],
                stdout=subprocess.PIPE,
                universal_newlines=True
            )
            afni_version = p.communicate()[0].split('\n')[0]
            if 'AFNI' in afni_version:
                afni_version = afni_version[afni_version.find('AFNI') + 5:]
            elif 'Version' in afni_version:
                afni_version = afni_version[afni_version.find('Version') + 8:]
        except (subprocess.CalledProcessError, OSError) as e:
            afni_version = '<span style="color:red">UNKNOWN(%s)</span>' % e

        msg = """<h3>A Tool for fBIRN fMRI QA Analysis</h3>
                 <p>Version: %s</p>
                 <p>Python Version: %s</p>
                 <p>BXH Version: %s</p>
                 <p>Afni Version: %s</p>
        """ % (__version__, python_version, bxh_version, afni_version)

        QMessageBox.about(self, "About the FBIRN Tool", msg.strip())

    def closeEvent(self, event):
        """Clean up temporary directories and files on main window close."""
        for d in [self.imagedir, self.qadir]:
            if d is not None:
                shutil.rmtree(d, ignore_errors=True)
        for f in [self.wrapped]:
            if f is not None:
                os.unlink(f)
        self.deleteLater()
        return

    def convert_to_bxh(self, indir, outdir, patterns=(r'[0-9]*', r'*.dcm')):
        """Run the BXH conversion program from dicom to xcede format."""
        files = []
        for pattern in patterns:
            files += glob(join(indir, pattern))
        if not files:
            raise IOError("No dicom files found in '%s'" % indir)
        cmdline = [self.DICOM2BXH, '--xcede'] + files + [outdir]
        subprocess.check_call(cmdline, stdout=open(os.devnull, 'w'))

    def tar_blob(self, dirname):
        """
        Tar up a directory as a binary object.

        Tar file is relative to dirname.
        """
        buf = BytesIO()
        tfile = tarfile.open(fileobj=buf, mode='w:gz')
        tfile.add(dirname, arcname='.', recursive=True)
        tfile.close()
        bufval = buf.getvalue()
        buf.close()
        return bufval

    def untar_blob(self, blob, dirname):
        """
        Untar a binary object as directory.

        Tar file expected to be relative.
        """
        buf = BytesIO(blob)
        tfile = tarfile.open(fileobj=buf, mode='r:gz')
        tfile.extractall(path=dirname)

    def list_qadates(self):
        """
        Get a list of the dates of previous QA runs.

        Returns a list of datetime objects.
        """
        dbtable = self._scanner_to_dbtable(self.current_scanner)
        if not dbtable:
            QMessageBox.critical(self, 'Results database', 'No valid Scanner')
            return []
        connection = None
        try:
            connection = psycopg2.connect(
                database=self.DBNAME, host=self.DBHOST
            )
            cursor = connection.cursor()
            cursor.execute('SELECT scandate FROM ' + dbtable)
            dates = [date for (date,) in cursor.fetchall()]
            cursor.close()
        except psycopg2.OperationalError as e:
            QMessageBox.critical(
                self, 'Results database',
                'Unable to access QA database %s (%s)' % (self.DBNAME, e)
            )
            return []
        finally:
            if connection is not None:
                connection.close()
        return dates

    def get_blob(self, qadate):
        """
        Return the tarfile blob for a previous QA run.

        The run is specified as a datetime object.
        """
        dbtable = self._scanner_to_dbtable(self.current_scanner)
        connection = None
        try:
            connection = psycopg2.connect(
                database=self.DBNAME, host=self.DBHOST
            )
            cursor = connection.cursor()
            cursor.execute(
                'SELECT qablob FROM ' + dbtable + ' WHERE scandate = %s',
                [qadate.strftime('%Y-%m-%d')]
            )
            blob = cursor.fetchone()[0]
            cursor.close()
        finally:
            if connection is not None:
                connection.close()
        return blob


class Usage(Exception):
    """Convenience for signalling to print Usage string."""

    def __init__(self, msg):
        Exception.__init__(self)
        self.msg = msg


def fetch_specified_series(patid, studyid, seriesno):
    """
    Run external command to get dicom series from server.

    Uses external dcm4che tools.
    Details of the server and the tools are currently hardwired.
    """
    server = 'CRICStore@canopus:11112'
    laet = os.uname()[1].split('.')[0] + 'Query'
    imagedir = tempfile.mkdtemp()
    cmdline = [
        '/usr/local/dcm4che2/bin/dcmqr',
        '-L', laet,
        server, '-S',
        '-cget', '-cstore', 'MR',
        '-qPatientID=%s' % patid,
        '-qStudyID=%s' % studyid,
        '-qSeriesNumber=%d' % seriesno,
        '-cstoredest', imagedir
    ]
    subprocess.check_call(cmdline)
    return imagedir


def main(argv=None):
    """
    Main entry point.

    Uses a 'passed in' argument list or sys.argv
    example: PQA12122011RHDA, 1, 3
    """
    # fix up path for if we are running as su
    syspath = os.environ["PATH"].split(":")
    if '/usr/local/afni/bin' not in syspath:
        syspath += ['/usr/local/afni/bin']
        os.environ["PATH"] = ':'.join(syspath)

    patid = None
    studyid = None
    seriesno = None

    if argv is None:
        argv = sys.argv
    progname = basename(argv[0])

    try:
        try:
            opts, args = getopt.getopt(
                argv[1:], 'hp:t:s:',
                ['help', 'patid', 'studyid', 'seriesno']
            )
        except getopt.error as msg:
            raise Usage(msg)

        for opt, arg in opts:
            if opt in ('-h', '--help'):
                print(__doc__)
                return 0
            if opt in ('-p', '--patid'):
                patid = arg
            if opt in ('-t', '--studyid'):
                studyid = arg
            if opt in ('-s', '--seriesno'):
                seriesno = int(arg)

        if QApplication.instance() is None:
            app = QApplication(argv)
        else:
            app = QApplication.instance()

        # Boiler-plate code to handle Ctrl-C cleanly
        signal.signal(signal.SIGINT, lambda sig, frame: QApplication.quit())
        t = QTimer(); t.start(500); t.timeout.connect(lambda: None)

        if patid is not None and studyid is not None and seriesno is not None:
            try:
                imagedir = fetch_specified_series(patid, studyid, seriesno)
            except IOError as e:
                print(
                    '%s: Unable to fetch dicom series for %s, %s, %d' %
                    (progname, patid, studyid, seriesno), file=sys.stderr
                )
                imagedir = None
            try:
                main_window = FBirnMainWindow(imagedir)
            except (IOError, subprocess.CalledProcessError) as e:
                print(
                    '%s: Unable to start up (%s)' % (progname, e),
                    file=sys.stderr
                )
                return 2
        else:
            main_window = FBirnMainWindow()

        if (    not isfile(main_window.DICOM2BXH) or
                not os.access(main_window.DICOM2BXH, os.X_OK) or
                not isfile(main_window.QAPERLSCRIPT) or
                not os.access(main_window.QAPERLSCRIPT, os.X_OK)):
            print('%s: Unable to find bxh tools!' % progname, file=sys.stderr)
            return 2

        main_window.show()
        main_window.setWindowIcon(QIcon(':images/fbirn.png'))
        return app.exec_()
    except Usage as err:
        print(err.msg, file=sys.stderr)
        print('for help use --help', file=sys.stderr)
        return 2


# BEGIN-RESOURCE-IMPORT
try:
    from . import icon_resources
except (ValueError, ImportError):
    pass
# END-RESOURCE-IMPORT
# MERGE-RESOURCE-HERE


if __name__ == '__main__':
    sys.exit(main())
