# Assessment of EPI Stability for fMRI #

## Introduction ##
As functional MRI investigations depend on small signal variations over an extended timecourse and rapid imaging sequence used ([EPI](http://practicalfmri.blogspot.co.uk/2011/11/physics-for-understanding-fmri.html)) is
highly susceptible to artefacts from system imperfections, we have established a protocol for monitoring the performance of EPI sequences.
 
There are protocols used by FBIRN which are an adaptation of the published methods of [Friedman and Glover 2006](http://www.ncbi.nlm.nih.gov/pubmed/16649196)

There are also newer methods by [Greve etal 2011](http://www.ncbi.nlm.nih.gov/pubmed/21413069)  which produce a metric in terms of the proportion of the
temporal noise arising from the MR system as opposed to the physiological fluctuations.

 
 - [BIRN](http://www.birncommunity.org/tools-catalog/function-birn-stability-phantom-qa-procedures/)

 - [New BHX Xcede Tools](http://www.nitrc.org/projects/bxh_xcede_tools)

 - [BHX Tools Docs](https://xwiki.nbirn.org:8443/bin/view/Function-BIRN/AutomatedQA)

## Phantom Construction ##
The phantom is based on the one used at fBIRN. It is a perspex sphere 17cm internal diameter composed of two plastic hemispheres (marketed as christmas tree decorations). This can be scanned easily in the 32 channel head coil.

BIRN provide instructions for [constructing the phantom](https://xwiki.nbirn.org:8443/bin/view/Function-BIRN/What+is+the+agar+phantom+for+and+what+do+I+do+with+it) - fBIRN uses 17.5 cm diameter phantom. The phantom is filled with a nickel doped agar gel. Care needs to be taken to remove bubbles (though note that fBIRN consider some bubbles to be acceptable as they are stable features). Also note that the solution temperature of the gel is quite high (70-80 &deg;C) and it is quite easy to melt the container when trying to melt the gel.

The two halves of the spheres need to be glued together (probably epoxy would be best) and a hole needs to be drilled for filling. The phantom needs to be sealed well to prevent water loss and dessication of the gel. Some water may be added in any cavity at the top of the gel (due to shrinkage) in order to help maintain the gel hydration and reduce the susceptibility artefact associated with the departure from an accurate spherical shape.

*detailed local build instructions here*
 
## Scanning Protocol ##
We want a protocol that is similar to that used by fBIRN but is applicable to a volunteer scan as well. These are some [parameters of protocols](EPIStabilityParameters.md) that have been used at various places.

The phantom should be set up and scanned according to a [standard procedure](EPIStabilityProtocol.md)  and the scans sent to the [DICOM Store on canopus](http://canopus.cric.bris.ac.uk/dcm4chee-web/).

## Data Analysis ##
The [fBIRN analysis](https://xwiki.nbirn.org:8443/bin/view/Function-BIRN/AutomatedQA) is based on [Friedman and Glover 2006](http://www.ncbi.nlm.nih.gov/pubmed/16649196). FBIRN provide a [C](http://en.wikipedia.org/wiki/C_%28programming_language%29) and [Perl](http://www.perl.org/) reimplementation of a historical [matlab](http://en.wikipedia.org/wiki/MATLAB) script implementing this analysis. We currently wrap a minimally modified version of the C/Perl code in a [graphical tool](wiki:EPIStabilityGUITool) which can access a [DICOM](http://en.wikipedia.org/wiki/DICOM) archive and saves the summary results to a [postgres](http://www.postgresql.org/) database on `canopus`.

### Image Series Analysis ###
These are synthetic images created by analysis of the intensity time series on
a pixel by pixel basis at a single slice position. For this purpose an approximately central slice is chosen.

#### Signal Image ####
Simple pixel by pixel average over time:

$$S_{ij} = \mu_{ij}$$

#### Detrending ####
Quadratic detrending is applied (individually) to each pixel before analysing the time series.

#### Temporal Fluctuation Noise Image ####
The standard deviation of the temporal variation of each pixel after detrending:

$$TFN_{ij} = \sigma_{ij}$$

#### Signal to Fluctuation Noise Ratio (SFNR) Image ####
Obtained by dividing the mean signal image by the Temporal Fluctuation Noise Image:

$$SFNR_{ij} = \frac{S_{ij}}{TFN_{ij}}$$

#### Static Spatial Noise Image ####
The average difference on a pixel by pixel basis of adjacent frames:

$$
SSN_{ij} = \Sigma_{n}{\left(I_{ij,2n+1} -  I_{ij,2n}\right)}
$$

#### ROI Analysis ####
For a 15x15 ROI centred in the middle of the selected slice, the signal intensity and variance are derived as a function of time.

#### SNR ####
Calculated using the mean image for the signal and the adjacent frame differences for the noise value. The value is scaled by the square root of the number of time points.

$$SNR = \frac{\overline{Signal}}{\sqrt{Var}} \cdot \frac{1}{\sqrt{ntimes}}$$

Should there not be a $\sqrt{2}$ factor here somewhere? - check green book. Actually, this might drop out as we've averaged n/2 ''pairs''.

#### SFNR ####
The average value of the SFNR Image over the 15*15 ROI.


#### Percentage Fluctuation and Drift ####
Using the time series of average intensity detrended *as a whole* with a quadratic fitting polynomial, the standard deviation of the residuals
is compared to the mean signal value prior to detrending. Percentage fluctuation is:

$$ PF = \frac{\sigma}{\overline{signal}} \times 100 \% $$

Drift is caculated as the range of values in the fit divided by the mean signal before detrending:

$$ Drift = \frac{maxfit - minfit}{\overline{signal}} \times 100 \% $$

#### ROI Stability ####
Using the time series of average intensity detrended *as a whole* with a quadratic fitting polynomial, the mean ROI signal time course is analysed.

#### Fourier Analysis ####
A mixed radix FFT is applied to the residuals and the *magnitude* spectrum is plotted. This is intended to show individual sources of temporal fluctuation
such as the cold head pump or gradient mechanical resonances. The spectrum is scaled to the average signal intensity.


### Weisskoff Analysis ###
This analysis based on [Weisskoff 2005](http://onlinelibrary.wiley.com/doi/10.1002/mrm.1910360422/abstract) takes time series of average signal intensity for a range of different ROI sizes and plots (on a log-log scale) the Coefficient of Variation (normalised SD) *wrt* to time against the
size of the ROI in pixels. The expected relation in the absence of additional correlations is a straight line. However, system instabilities leads to a plateau in the plot.
A *Radius of Decorrelation* is defined to characterise this:

$$R_{DC} = \frac{CV_{1}}{CV_{N_{max}}}$$

Although unclear from Friedman and Glover it seems the time series are detrended before calculating the Coefficient of Variation.


### Image Smoothness (FWHM) ###
This uses [Afni](http://afni.nimh.nih.gov/afni/) tool `3dFWHM` to determine how the image smoothness varies along the time series.


### Positional Stability (Centre of Mass) ###
This is done by determining the position of the spherical phantom in three coordinates at each time point and plotting the positions as a function of time.
It is done in two ways: firstly with a simple centoid method and then using the [Afni](http://afni.nimh.nih.gov/afni/) tool `3dvolreg`
The variability here is usually very small, presumably the variation in the epi low bandwidth direction is compensated in the Siemens sequence.

### Ghostiness ###
This is an assessment of the severity of ghosting in the images. It is generated by comparing the signal intensity in background regions liable to ghosting with
the signal in the phantom. This is done by manipulating image masks.
The mean intensity of the voxels in the *ghost* mask and the mean intensity of the worst ten percent of these are plotted as a function of time.

### Transmitter & Receiver Gains and Centre Frequency ###
We don't currently measure these though they are available from the private part of the DICOM header.

### Tools ###

#### BIRN Tools ####
BIRN provide the [BHX Xcede tools](https://xwiki.nbirn.org:8443/bin/view/Function-BIRN/AutomatedQA) for analysis of the phantom scans.
We have used this software and provided a suitable [user interface](EPIStabilityGUITool.md) for accessing scans from a DICOM server and saving the results of the analysis to a database.


#### Independent Analysis ####
In addition, we have written native python/numpy routines to validate the analysis that are available in [Jupyter Notebook](http://jupyter.org/) form which may be viewed statically [here](http://nbviewer.ipython.org/4088430). The [original](https://gist.github.com/4088430) is also downloadable and can be run on any workstation.


### Historical Data ###
There is a live [http://canopus.cric.bris.ac.uk/cgi-bin/gchart_fmriqa_report.py google charts plot of the historical data] available on the `canopus` web server. There is also a another notebook with more detailed plots available as a [static view](http://nbviewer.ipython.org/4088460) or a [download](https://gist.github.com/4088460).

 
### Initial Observations ###
 - Gain variation is slight but follows a non-linear course, rising initially before turning over onto a descending trend line.
 - On some scans small but significant medium period ripples which could be seen in the time series.
 - There is no noticeable peak in the (Fourier) spectrum at higher frequencies.
 - There is very little variation in smoothness (FWHM) other than a small transient over the first 5-10 time points.
 - There is some irregularity in estimation the z component of FWHM where the algorithm breaks down. These points are omitted from the plots.
 - There is very little positional drift.
 - The Weisskoff plots seem irregular. This may be a phantom issue.
 - The derived *radii of decorrelation* are somewhat smaller than expected and quite variable.
 - The Nyquist (N/2) ghosting is quite noticeable at an average brightness of 3-4% but increases only slightly with time though the scan.

## Trends ##

## Volunteer Scans and Noise Analysis ##
**(under development)**

Following [Greve etal 2011](http://www.ncbi.nlm.nih.gov/pubmed/21413069) we try and characterise the system fluctuation noise as a proportion of physiological variability
by performing volunteer scans (repeatedly on the same volunteer) with the same protocol as is used on the phantom. In this case the following parameters are used:

### EPI Scans
|Protocol |Seq |TR  |TE|FoV|Matrix|BW  |Frames|Pix Size|Slices|Coil-Comb|Coverage|&alpha;            |PE  |Phase BW|Shim-Mode|iPAT|DistortionCorrn|RampSampling|Filter|FatSat|
|---------|----|----|--|---|------|----|------|--------|------|---------|--------|-------------------|----|--------|---------|----|---------------|------------|------|------|
|Phantom  |EPI |2000|30|220|64*64 |2298|100/50|3.44    |4+1   |RMS      |        |10&deg; and 77&deg;|P->A|??      |Advanced |None|Off            |Yes         |None  |Yes   |
|Volunteer|EPI |2000|30|220|64*64 |2298|100/50|3.44    |4+1   |RMS      |        |10&deg; and 77&deg;|P->A|??      |Advanced |None|Off            |Yes         |None  |Yes   |
|         |    |    |  |   |      |    |      |        |      |         |        |                   |    |        |         |    |               |            |      |      |


### Anatomical Scans

|Protocol|Seq    |TR  |TI  |TE  |FoV|Matrix|BW|Pix Size|Slices|&alpha;|PE|Shim-Mode|iPAT|DistortionCorrn|Filter|FatSat|
|--------|-------|----|----|----|---|------|--|--------|------|-------|--|---------|----|---------------|------|------|
|Anatomy |MP-RAGE|2030|1100|2.94|   |      |  |0.86    |1.2   |10&deg;|  |         |    |               |      |      |
|        |       |    |    |    |   |      |  |        |      |       |  |         |    |               |      |      |

